#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt-get update

apt-get -y install git vim-tiny
apt-get -y install unattended-upgrades
apt-get -y install dnsutils netcat
apt-get -y install python3-pip
apt-get -y install prometheus-node-exporter prometheus-node-exporter-collectors
apt-get -y install puppet-module-puppetlabs-mount-core

update-alternatives --set editor /usr/bin/vim.tiny


