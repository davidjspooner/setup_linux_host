#!/bin/bash

rm -rf /usr/local/go 
rm go1.20.5.linux-arm64.tar.gz
wget https://go.dev/dl/go1.20.5.linux-arm64.tar.gz
tar -C /usr/local -xzf go1.20.5.linux-arm64.tar.gz

echo "export PATH=$PATH:/usr/local/go/bin" > /etc/profile.d/90_golang.sh
