#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt -y install puppet

puppet apply << __EOF__


    file { "/mnt/joletta":
            ensure => directory,
            owner   => "root",
            group  => "root",
            mode   => "755",
    }

    mount { "/mnt/joletta":
            device   => "192.168.3.24:/volume1/jolettadev",
            atboot   => yes,
            fstype   => "nfs",
            options  => "tcp,hard,intr,rw,bg",
            ensure   => mounted,
            remounts => true,
            pass     => "0",
            require => File["/mnt/joletta"], 
    }

__EOF__

rfkill block 0
