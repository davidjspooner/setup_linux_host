#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
apt -y install puppet

puppet apply << __EOF__


    file { "/var/opt/dsbox":
            ensure => directory,
            owner   => "root",
            group  => "root",
            mode   => "755",
    }

    mount { "/var/opt/dsbox":
            device   => "192.168.3.24:/volume1/pitray",
            atboot   => yes,
            fstype   => "nfs",
            options  => "tcp,hard,intr,rw,bg",
            ensure   => mounted,
            remounts => true,
            pass     => "0",
            require => File["/var/opt/dsbox"], 
    }

__EOF__

#apt -y install /var/opt/dsbox/repo/dsprompt-1.1.5.deb
#apt -y install /var/opt/dsbox/repo/dslight-5.0.75.deb

