#!/bin/bash

ln -s /mnt/joletta/workspace ~/workspace
cd ~/workspace
apt install docker-compose
cd docker-compose
docker-compose up --detach
